<!DOCTYPE html>
<html>
<head>
	<title>Barcode Try</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

	<?php
		require "vendor/autoload.php";
		 
		try
		{
		 // create a PostgreSQL database connection
		 $pdo = new PDO('mysql:host=localhost;dbname=uapv;charset=utf8', 'root', 'secret');
		 
		 // display a message if connected to the PostgreSQL successfully
		 if($pdo)
		 {
		 	echo "Connected to the database <b>successfully</b> !";
		 }
		}
		catch (PDOException $e)
		{
		 // report error message
		 echo $e->getMessage();
		}
	?>
</head>
<body style="text-align: center;">

	

	<table class="table table-striped" style="text-align: center;">
	  <thead>

	    <tr style="text-transform: uppercase; font-weight: 700;">
	      <th scope="col">Nom produit</th>
	      <th scope="col">Prix</th>
	      <th scope="col">Code produit</th>
	    </tr>

	  </thead>
	  <tbody>

	    <?php
	  		$stmt = $pdo->query('SELECT distinct code_produit, nom, prix FROM produits order by code_produit ASC;');

			while ($row = $stmt->fetch()):

				$Bar = new Picqer\Barcode\BarcodeGeneratorPNG();

				$string = $row['code_produit'];
	  	?>

		    <tr style="font-weight: 300; text-transform: capitalize;">
				<th scope="col"><?php echo $row['nom']; ?></th>
				<th scope="col"><?php echo $row['prix']." €"; ?></th>
			    <th scope="col">
					<?php echo '<img src="data:image/png;base64,' . base64_encode($Bar->getBarcode($string, $Bar::TYPE_CODE_128)) . '">'; ?>
				</th>
		    </tr>

		<?php endwhile; ?>

	  </tbody>
	</table>

</body>
</html>